<?php

namespace App\Middleware;

use Slim\Interfaces\RouterInterface;

class AuthenticatedMiddleware
{
  // Slim $app container
  protected $router;

  public function __construct(RouterInterface $router) {
    $this->router = $router;
  }

  public function __invoke($request, $response, $next) {
    if(!isset($_SESSION['VERITY'])) {
      $response = $response->withRedirect('/portal18?des=translim');
    }
    return $next($request, $response);
  }

}
