<?php

use \App\Controllers\TopicController;
use \App\Middleware\IpFilter;
use \App\Middleware\AuthenticatedMiddleware;

// Add GLOBAL Middleware
// $app->add(new IpFilter($container['db']));


$mw = function($request, $response, $next){
  $response->getBody()->write('FUNCTION MIDDLEWARE');
  return $next($request, $response);
};
// print_r($container);
// exit;
$app->group('', function() use ($app, $container) {
  // $app->get('/', function($request, $response) {
  //   // $response = $response->withRedirect($this->router->pathFor('login'));
  //   // return $response;
  //   // return 'Home';
  //   return $response->getBody()->write('<br/>Hello, World<br/>');
  // })->add(new AuthenticatedMiddleware($container['router']));

  $app->get('/login', function() {
    return 'Login';
  })->setName('login');
})->add($mw);

// NOTE: TESTING ROUTES ONLY...
//
// This is just a test of Doctrine DBAL
//$app->get('/dbal', function($request, $response) use ($conn) {
  //$sql = "SELECT EFFDT FROM SYSADM.PS_UI_PERS_PROFILE WHERE EMPLID=?";
  //$stmt = $this->get('conn')->prepare($sql);
  //$stmt->bindValue(1, 1018863);
  //$stmt->execute();

  //while ($row = $stmt->fetch()) {
    //echo $row['EFFDT'];
  //}
  //exit;
//});

// Testing Validations...
$app->get('/', function($request, $response) {
  return $this->view->render($response, 'home.twig');
})->setName('home');

use Respect\Validation\Validator as v;
$app->post('/signup', function($request, $response) {
  // TODO: validation
  // ohno($this);
  // ohno($this->validator);
  $validation = $this->validator->validate($request, [
    'username' => v::notEmpty()->noWhitespace()->isSameAs('joe'),
  ]);

  if ($validation->failed()) {
    // redirect to previous page and show errors
    return $response->withRedirect($this->router->pathFor('home'));
  }

  die('register user');
})->setName('signup');
