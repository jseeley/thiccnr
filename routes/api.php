<?php

// use \App\Controllers\TopicController;
// use \App\Middleware\IpFilter;

// Add GLOBAL Middleware
// $app->add(new IpFilter($container['db']));

$app->group('/api', function (App $app) {
  $app->get('/', function() {
    return 'Home';
  });

  $app->get('/login', function() {
    return 'Login';
  });
});
